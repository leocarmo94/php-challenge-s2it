const Components = {};
(function (comp) {


    /**
     * Load session message in Toastjs
     * @param status
     * @param message
     */
    comp.LoadToast = function (status, message) {

        toastr.options = {
            "closeButton": true,
            "progressBar": true,
            "timeOut": "8000"
        };

        toastr[status](message);

    };

    /**
     * Create FormData for a form submit
     * @param form_for_search
     * @returns {FormData}
     */
    comp.formData = function(form_for_search) {

        let form_element = $(form_for_search)[0];

        let formData = new FormData();

        // fetch form values
        for (let i = 0; i < form_element.elements.length; i++) {

            let form_input = form_element.elements[i];

            if (form_input.value === '')
                continue;

            try {

                switch (form_input.type) {

                    case 'file':
                        formData.append(form_input.name, document.getElementById(form_input.id).files[0]);
                        break;

                    case 'checkbox':
                    case 'radio':
                        if (form_input.checked)
                            formData.append(form_input.name, form_input.value);
                        break;

                    default:
                        formData.append(form_input.name, form_input.value);
                        break;

                }

            } catch (e) {

                console.log(form_input.id + ' invalid element id', e)

            }

        }

        return formData;

    };

    comp.toastAllMessages = function (type, messages) {

        // render errors
        if (messages instanceof Array || typeof messages === 'object') {

            for (let msgs in messages) {

                if (messages.hasOwnProperty(msgs)) {

                    for (let i = 0; i < messages[msgs].length; i++) {
                        new Components.LoadToast(type, messages[msgs][i]);
                    }

                }
            }

        } else {

            new Components.LoadToast(type, messages);

        }

    };


})(Components);