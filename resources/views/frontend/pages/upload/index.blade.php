@extends('frontend.layout.main')

@section('container')
    <div class="column is-4 is-offset-4">

        <h1 class="title has-text-white">Process XML</h1>
        <p class="subtitle has-text-white">Select XML file and process it</p>

        <div class="box">
            <div class="has-text-left">

                <form id="xml_process_form" @submit.prevent="process" method="POST">

                    <div class="field">
                        <label class="label">XML File</label>
                        <div class="control">
                            <input type="file" id="xml_file" name="xml_file" class="dropify" data-allowed-file-extensions="xml" required>
                        </div>
                    </div>

                    <div class="field">
                        <label class="label">Model type</label>
                        <label class="radio">
                            <input type="radio" id="model" name="model" value="people" v-model="model" required>
                            People
                        </label>
                        <label class="radio">
                            <input type="radio" id="model" name="model" value="ship_order" v-model="model" required>
                            Ship Orders
                        </label>
                    </div>

                    <div class="field">
                        <button
                            class="button is-block is-info is-large is-fullwidth"
                            :class="{'is-loading': loading}"
                        ><i class="fas fa-cog"></i> Process</button>
                    </div>

                </form>

            </div>
        </div>

        <p class="has-text-white-ter">
            @ <a href="https://leonardocarmo.com.br/" target="_blank" class="has-text-white">Leonardo Carmo</a>
        </p>

    </div>
@endsection

@section('script')
    <script>

        const ROUTE_PROCESS_PEOPLE = '{{ route('frontend.upload.people.process') }}';
        const ROUTE_PROCESS_SHIP_ORDER = '{{ route('frontend.upload.ship_order.process') }}';

    </script>
    <script src="/assets/js/pages/upload.index.js"></script>
@endsection