<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Process XML - Leonardo Carmo</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="{{ mix('assets/css/app.css') }}" rel="stylesheet">

</head>

<body>
<section id="app" class="hero is-fullheight">
    <div class="hero-body">
        <div class="container has-text-centered">

            @yield('container')

        </div>
    </div>
</section>

<script src="{{ mix('assets/js/app.js') }}"></script>
@yield('script')

</body>

</html>