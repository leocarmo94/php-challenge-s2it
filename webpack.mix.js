let mix = require('laravel-mix');

mix.styles([

    // Lib
    'resources/assets/css/lib/*',

    // Custom
    'resources/assets/css/custom/style.css'

], 'public/assets/css/app.css').version();

mix.babel([

    // Primary
    'resources/assets/js/primary/jquery-3.3.1.min.js',
    'resources/assets/js/primary/components.js',

    // LIBS
    'resources/assets/js/lib/*',

], 'public/assets/js/app.js').version();

