<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('frontend.upload');
});

Route::group([
    'namespace' => 'Frontend',
], function () {

    Route::group([
        'prefix' => '/upload',
    ], function () {

        Route::view('/', 'frontend.pages.upload.index')->name('frontend.upload');

        Route::post('/people/process', 'PeopleController@process')->name('frontend.upload.people.process');
        Route::post('/ship-order/process', 'ShipOrderController@process')->name('frontend.upload.ship_order.process');

    });

});