<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1',
    'namespace' => 'V1'
], function () {

    Route::group([
        'prefix' => '/auth'
    ], function () {

        Route::post('/', 'AuthController@auth')->name('api.auth');

        Route::group([
            'middleware' => 'auth.api'
        ], function () {

            Route::post('/me', 'AuthController@me')->name('api.auth.me');

            Route::post('/token/refresh', 'AuthTokenController@refresh')->name('api.auth.token.refresh');

        });

    });

    Route::group([
        'middleware' => 'auth.api'
    ], function () {

        Route::get('/people', 'PeopleController@index')->name('api.people');
        Route::get('/people/{id}', 'PeopleController@show')->name('api.people.show');

        Route::get('/ship-order', 'ShipOrderController@index')->name('api.ship_order');
        Route::get('/ship-order/{id}', 'ShipOrderController@show')->name('api.ship_order.show');

    });

});
