# XML upload and REST API with JWT Auth

With this application, you can upload XML files and get results via REST

## Installing

First, clone this repo and install the [composer](https://getcomposer.org/) dependencies:
```sh
composer install
```

Copy the `.env.example` to `.env` to configure the app:
```sh
cp .env.example .env
``` 

Generate Laravel key and JWT key:
```sh
php artisan key:generate
php artisan jwt:secret
``` 

Run migrations with seeds:
```sh
php artisan migrate --seed
``` 

## XML Upload

Now, access the home page and you will be redirected to upload page.
Select XML valid file, the model type for upload and process the file.

## REST API v1

Main endpoint for all requests: `/api/v1`

##### Auth

First, you need JWT Token for make requests
```
POST /auth

Parameters
[email] admin@admin.com
[password] secret

Response
{
    "token": "JWT_TOKEN",
    "user": {
        "id": 1,
        "name": "Admin",
        "email": "admin@admin.com",
        "created_at": "2018-08-15 21:12:30",
        "updated_at": "2018-08-15 21:12:30"
    }
}
``` 

After getting your token, all requests require this header:
```
Authorization: Bearer JWT_TOKEN
``` 

Refresh token
```
POST /auth/token/refresh

Response
{
    "token": "NEW_JWT_TOKEN"
}
``` 

##### People

For retrieve all, send request without {id}
```
GET /people/{?id}

Response
[
    {
        "id": 1,
        "name": "Name 1",
        "created_at": "2018-08-15 20:29:34",
        "updated_at": "2018-08-15 20:29:34",
        "phone": [
            {
                "id": 39,
                "person_id": 1,
                "phone": "2345678",
                "created_at": "2018-08-15 20:29:34",
                "updated_at": "2018-08-15 20:29:34"
            },
            {
                "id": 40,
                "person_id": 1,
                "phone": "1234567",
                "created_at": "2018-08-15 20:29:34",
                "updated_at": "2018-08-15 20:29:34"
            }
        ]
    },
    {...}
]
``` 

##### Ship Order

For retrieve all, send request without {id}
```
GET /ship-order/{?id}

Response
[
    {
        "id": 1,
        "person_id": 1,
        "created_at": "2018-08-15 20:30:05",
        "updated_at": "2018-08-15 20:30:05",
        "person": {
            "id": 1,
            "name": "Name 1",
            "created_at": "2018-08-15 20:29:34",
            "updated_at": "2018-08-15 20:29:34"
        },
        "ship_to": {
            "id": 16,
            "ship_order_id": 1,
            "name": "Name 1",
            "address": "Address 1",
            "city": "City 1",
            "country": "Country 1",
            "created_at": "2018-08-15 20:30:05",
            "updated_at": "2018-08-15 20:30:05"
        },
        "items": [
            {
                "id": 19,
                "ship_order_id": 1,
                "title": "Title 1",
                "note": "Note 1",
                "quantity": 745,
                "price": 123.45,
                "created_at": "2018-08-15 20:30:05",
                "updated_at": "2018-08-15 20:30:05"
            }
        ]
    },
    {...}
]
``` 

## Testing

Create sqlite database for tests, migrate and seed:
```sh
touch ./database/database_test.sqlite
php artisan migrate --database=sqlite_test --seed
``` 

Now, just run tests.

```sh
vendor/bin/phpunit
``` 

*Important, before testing, populate main database with XML files for assert all tests*

## Credits

- [Leonardo do Carmo](https://github.com/leocarmo)