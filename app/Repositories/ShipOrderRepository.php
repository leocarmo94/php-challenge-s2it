<?php

namespace App\Repositories;

use App\Models\ShipOrder;
use Illuminate\Support\Facades\Cache;

class ShipOrderRepository
{

    public static function all()
    {
        return Cache::remember('people.all', env('CACHE_MINUTES'), function () {

            return ShipOrder::with(['person', 'shipTo', 'items'])
                ->get();

        });
    }

    public static function findOrFail(int $id)
    {
        return Cache::remember("people.findOrFail.$id", env('CACHE_MINUTES'), function () use ($id) {

            return ShipOrder::with(['person', 'shipTo', 'items'])
                ->find($id);

        });
    }

}