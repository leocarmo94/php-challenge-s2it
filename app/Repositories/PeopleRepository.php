<?php

namespace App\Repositories;

use App\Models\Person;
use Illuminate\Support\Facades\Cache;

class PeopleRepository
{

    public static function all()
    {
        return Cache::remember('people.all', env('CACHE_MINUTES'), function () {

            return Person::with('phone')
                ->get();

        });
    }

    public static function findOrFail(int $id)
    {
        return Cache::remember("people.findOrFail.$id", env('CACHE_MINUTES'), function () use ($id) {

            return Person::with('phone')
                ->find($id);

        });
    }

}