<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class XmlService
{

    /**
     * @var UploadedFile
     */
    protected $file_bag;

    /**
     * @var string
     */
    protected $saved_file_path;

    /**
     * XmlService constructor
     * @param UploadedFile $file_bag
     */
    private function __construct(UploadedFile $file_bag)
    {
        $this->file_bag = $file_bag;
    }

    /**
     * @param UploadedFile $file_bag
     * @return XmlService
     */
    public static function file(UploadedFile $file_bag)
    {
        return new self($file_bag);
    }

    /**
     * @return $this
     */
    public function store()
    {
        $saved_file = $this->file_bag->storeAs(
            'xml',
            Carbon::now()->format('YmdHis') . '.xml'
        );

        $this->saved_file_path = storage_path(
            "app/$saved_file"
        );

        return $this;
    }

    /**
     * @return \SimpleXMLElement
     * @throws \Exception
     */
    public function parse()
    {
        try {

            return simplexml_load_file($this->saved_file_path);

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            throw new \Exception(
                'Error while parsing XML file, check if the syntax is correct!',
                415
            );

        }
    }

    /**
     * @return string
     */
    public function getSavedFilePath()
    {
        return $this->saved_file_path;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setFakeFilePathForTest(string $path)
    {
        $this->saved_file_path = $path;
        return $this;
    }

}