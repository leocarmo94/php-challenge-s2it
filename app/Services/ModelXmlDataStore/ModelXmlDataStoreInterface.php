<?php

namespace App\Services\ModelXmlDataStore;

interface ModelXmlDataStoreInterface
{

    public function process();

    public function store();

}