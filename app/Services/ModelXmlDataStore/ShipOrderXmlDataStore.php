<?php

namespace App\Services\ModelXmlDataStore;

use App\Models\ShipOrder;
use App\Models\ShipOrderItem;
use App\Models\ShipOrderTo;

final class ShipOrderXmlDataStore extends ModelXmlDataStore
{

    /**
     * @var array
     */
    protected $ship_order = [];

    /**
     * @var array
     */
    protected $ship_order_to = [];

    /**
     * @var array
     */
    protected $ship_order_items = [];

    /**
     * @return bool
     */
    protected function fillModel()
    {
        $ship_order = ShipOrder::insert(
            $this->ship_order
        );

        $ship_order_to = ShipOrderTo::insert(
            $this->ship_order_to
        );

        $ship_order_items = ShipOrderItem::insert(
            $this->ship_order_items
        );

        return ($ship_order && $ship_order_to && $ship_order_items);
    }

    /**
     * @throws \Exception
     */
    protected function fillData()
    {
        foreach ($this->xml_element as $shiporder) {

            if (! $shiporder->orderid || ! $shiporder->orderperson)
                throw new \Exception('Parse error!');

            $this->ship_order[] = [
                'id' => (int) $shiporder->orderid,
                'person_id' => (int) $shiporder->orderperson,
                'created_at' => $this->getTimestamp(),
                'updated_at' => $this->getTimestamp()
            ];

            if ($shiporder->shipto) {
                $this->fillShipTo($shiporder);
            }

            if ($shiporder->items) {
                $this->fillItems($shiporder);
            }

        }
    }

    private function fillShipTo($shiporder)
    {
        foreach ($shiporder->shipto as $shipto) {

            $this->ship_order_to[] = [
                'ship_order_id' => (int) $shiporder->orderid,
                'name' => (string) $shipto->name,
                'address' => (string) $shipto->address,
                'city' => (string) $shipto->city,
                'country' => (string) $shipto->country,
                'created_at' => $this->getTimestamp(),
                'updated_at' => $this->getTimestamp()
            ];

        }
    }

    private function fillItems($shiporder)
    {
        foreach ($shiporder->items->item as $item) {

            $this->ship_order_items[] = [
                'ship_order_id' => (int) $shiporder->orderid,
                'title' => (string) $item->title,
                'note' => (string) $item->note,
                'quantity' => (int) $item->quantity,
                'price' => (float) $item->price,
                'created_at' => $this->getTimestamp(),
                'updated_at' => $this->getTimestamp()
            ];

        }
    }

}