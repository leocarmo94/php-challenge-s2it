<?php

namespace App\Services\ModelXmlDataStore;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

abstract class ModelXmlDataStore implements ModelXmlDataStoreInterface
{

    /**
     * @var \SimpleXMLElement
     */
    protected $xml_element;

    /**
     * @var Carbon
     */
    protected $timestamps;

    /**
     * @return mixed
     */
    abstract protected function fillData();

    /**
     * @return bool
     */
    abstract protected function fillModel();

    /**
     * @param \SimpleXMLElement $xml_element
     */
    public function __construct(\SimpleXMLElement $xml_element)
    {
        $this->xml_element = $xml_element;
    }

    /**
     * Process xml element
     *
     * @return $this
     * @throws \Exception
     */
    public function process()
    {
        try {

            $this->fillData();

            return $this;

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            throw new \Exception(
                'Error while process data, check if these XML file is corrected formatted!',
                500
            );

        }
    }

    /**
     * Store processed data in database
     *
     * @return bool
     * @throws \Exception
     */
    public function store()
    {
        try {

            return $this->fillModel();

        } catch (\Exception $e) {

            Log::error($e->getMessage());

            throw new \Exception(
                'Error while storing data in database, check if these registers were already inserted!',
                500
            );

        }
    }

    /**
     * Just for cache timestamp for fast processing
     *
     * @return Carbon
     */
    protected function getTimestamp()
    {
        if ($this->timestamps) {
            return $this->timestamps;
        }

        return $this->timestamps = Carbon::now();
    }

}