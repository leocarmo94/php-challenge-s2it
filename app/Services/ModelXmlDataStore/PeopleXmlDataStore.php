<?php

namespace App\Services\ModelXmlDataStore;

use App\Models\Person;
use App\Models\PersonPhone;

final class PeopleXmlDataStore extends ModelXmlDataStore
{

    /**
     * @var array
     */
    protected $people = [];

    /**
     * @var array
     */
    protected $person_phones = [];

    /**
     * @return bool
     */
    protected function fillModel()
    {
        $person = Person::insert(
            $this->people
        );

        $phone = PersonPhone::insert(
            $this->person_phones
        );

        return ($person && $phone);
    }

    /**
     * @throws \Exception
     */
    protected function fillData()
    {
        foreach ($this->xml_element as $person) {

            if (! $person->personid || ! $person->personname)
                throw new \Exception('Parse error!');

            $this->people[] = [
                'id' => (int) $person->personid,
                'name' => (string) $person->personname,
                'created_at' => $this->getTimestamp(),
                'updated_at' => $this->getTimestamp()
            ];

            if ($person->phones->phone) {
                $this->fillPersonPhones($person);
            }

        }
    }

    private function fillPersonPhones($person)
    {
        foreach ($person->phones->phone as $phone) {

            $this->person_phones[] = [
                'person_id' => (int) $person->personid,
                'phone' => (string) $phone,
                'created_at' => $this->getTimestamp(),
                'updated_at' => $this->getTimestamp()
            ];

        }
    }

}