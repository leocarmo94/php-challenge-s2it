<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Frontend\ProcessXmlRequest;
use App\Services\ModelXmlDataStore\PeopleXmlDataStore;
use App\Services\XmlService;
use App\Http\Controllers\Controller;

class PeopleController extends Controller
{

    public function process(ProcessXmlRequest $request)
    {
        try {

            $parsed_xml =
                XmlService::file($request->file('xml_file'))
                    ->store()
                    ->parse();

            $result = new PeopleXmlDataStore($parsed_xml);
            $result->process()
                    ->store();

            return response('Success. The registers were inserted!', 200);

        } catch (\Exception $e) {

            return response(
                ['errors' => $e->getMessage()],
                $e->getCode()
            );

        }
    }

}
