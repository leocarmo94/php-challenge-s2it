<?php

namespace App\Http\Controllers\Api\V1;

use App\Repositories\PeopleRepository;
use App\Http\Controllers\Controller;

class PeopleController extends Controller
{

    public function index()
    {
        return PeopleRepository::all();
    }

    public function show(int $id)
    {
        if ($people = PeopleRepository::findOrFail($id)) {
            return $people;
        }

        return response('Not found', 404);
    }

}
