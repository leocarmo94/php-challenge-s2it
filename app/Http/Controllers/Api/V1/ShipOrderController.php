<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\ShipOrderRepository;

class ShipOrderController extends Controller
{

    public function index()
    {
        return ShipOrderRepository::all();
    }

    public function show(int $id)
    {
        if ($ship_order = ShipOrderRepository::findOrFail($id)) {
            return $ship_order;
        }

        return response('Not found', 404);
    }

}
