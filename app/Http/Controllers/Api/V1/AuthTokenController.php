<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthTokenController extends Controller
{

    public function refresh()
    {
        if (! $token = JWTAuth::getToken()) {
            return response()->json(['token_not_found'], 401);
        }

        try {

            $token = JWTAuth::refresh();

        } catch (\Exception $e) {

            return response()->json([$e->getMessage()], 500);

        }

        return response()->json(compact('token'));
    }

}
