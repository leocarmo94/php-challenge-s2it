<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{

    public function phone()
    {
        return $this->hasMany(PersonPhone::class, 'person_id');
    }

}
