<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipOrder extends Model
{

    public function person()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

    public function shipTo()
    {
        return $this->hasOne(ShipOrderTo::class, 'ship_order_id');
    }

    public function items()
    {
        return $this->hasMany(ShipOrderItem::class, 'ship_order_id');
    }

}
