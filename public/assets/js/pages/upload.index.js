let vue = new Vue({

    el: '#app',

    data: {
        loading: false,
        model: '',
        routes: {
            people: ROUTE_PROCESS_PEOPLE,
            ship_order: ROUTE_PROCESS_SHIP_ORDER
        }
    },

    created() {

        $(document).ready(function () {

            $('.dropify').dropify();

        });

    },

    methods: {

        process() {

            this.loading = true;

            axios.post(
                this.routes[this.model],
                Components.formData('#xml_process_form'),
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            )
            .then(function (response) {

                console.log(response);

                new Components.toastAllMessages('success', response.data);

                document.getElementById("xml_process_form").reset();

                $('.dropify-clear').click();

            })
            .catch(function (error) {

                console.log(error.response.data);

                new Components.toastAllMessages('error', error.response.data.errors);

            })
            .finally(() => this.loading = false);

        }

    }

});