<?php

namespace Tests\Unit\Frontend;

use App\Services\XmlService;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class XmlServiceTest extends TestCase
{

    /**
     * @var string
     */
    protected $xml_valid_path_file;

    /**
     * @var XmlService
     */
    protected $xml_service;

    public function setUp()
    {
        parent::setUp();

        $this->xml_valid_path_file = storage_path('app/tests/people.xml');

        $this->xml_service = XmlService::file(
            UploadedFile::fake()->create('people.xml')
        );
    }

    public function testConstructClass()
    {
        $this->assertInstanceOf(XmlService::class, $this->xml_service);
    }

    public function testStore()
    {
        $this->xml_service->store();

        $this->assertFileExists(
            $this->xml_service->getSavedFilePath()
        );
    }

    public function testParseException()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Error while parsing XML file, check if the syntax is correct!');

        $this->xml_service->parse();
    }

    public function testParse()
    {
        $this->xml_service->setFakeFilePathForTest(
            $this->xml_valid_path_file
        );

        $xml_parsed = $this->xml_service->parse();

        $this->assertInternalType('object', $xml_parsed);
        $this->assertInstanceOf(\SimpleXMLElement::class, $xml_parsed);
    }

}
