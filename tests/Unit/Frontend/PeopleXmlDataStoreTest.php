<?php

namespace Tests\Unit\Frontend;

use App\Models\Person;
use App\Models\PersonPhone;
use App\Services\ModelXmlDataStore\PeopleXmlDataStore;
use App\Services\XmlService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class PeopleXmlDataStoreTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @var PeopleXmlDataStore
     */
    protected $people_xml_data_store;

    /**
     * @var XmlService
     */
    protected $xml_service;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $this->xml_service = XmlService::file(
            UploadedFile::fake()->create('people.xml')
        );

        $this->xml_service->setFakeFilePathForTest(
            storage_path('app/tests/people.xml')
        );

        $this->people_xml_data_store = new PeopleXmlDataStore(
            $this->xml_service->parse()
        );
    }

    /**
     * @throws \Exception
     */
    public function testConstruct()
    {
        $this->assertInstanceOf(
            PeopleXmlDataStore::class,
            $this->people_xml_data_store
        );
    }

    /**
     * @throws \Exception
     */
    public function testProcessFile()
    {
        $this->assertInstanceOf(
            PeopleXmlDataStore::class,
            $this->people_xml_data_store->process()
        );
    }

    /**
     * @throws \Exception
     */
    public function testExceptionProcessWrongFile()
    {
        $xml_service =
            XmlService::file(
                UploadedFile::fake()->create('invalid.xml')
            )
            ->setFakeFilePathForTest(
                storage_path('app/tests/shiporders.xml')
            );

        $process = new PeopleXmlDataStore(
            $xml_service->parse()
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Error while process data, check if these XML file is corrected formatted!');

        $process->process();
    }

    /**
     * @throws \Exception
     */
    public function testStoreData()
    {
        $this->assertTrue(
            $this->people_xml_data_store->process()->store()
        );
    }

    /**
     * @throws \Exception
     */
    public function testStoredData()
    {
        $this->people_xml_data_store->process()->store();

        $person = Person::first();

        $this->assertInstanceOf(
            Person::class,
            $person
        );

        $this->assertInstanceOf(
            PersonPhone::class,
            $person->phone->first()
        );
    }

}
