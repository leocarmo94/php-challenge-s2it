<?php

namespace Tests\Feature\Frontend;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AvailablePagesTest extends TestCase
{

    public function testHomeRedirectToUploadPage()
    {
        $response = $this->get('/');

        $response->assertStatus(302);
    }

    public function testUploadPageAvailable()
    {
        $response = $this->get(route('frontend.upload'));

        $response->assertStatus(200);
        $response->assertSee('Select XML file and process it');
    }

}
