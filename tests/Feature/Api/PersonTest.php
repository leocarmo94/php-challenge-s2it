<?php

namespace Tests\Feature\Api;

use App\Models\Person;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PersonTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * @var Client
     */
    protected $http;

    /**
     * @var string
     */
    protected $jwt_token;

    protected function setUp()
    {
        parent::setUp();

        $this->http = new Client([
            'base_uri' => env('APP_URL_TESTS')
        ]);

        // request for get jwt token
        $request = $this->http->post(
            route('api.auth', [], false),
            [
                'form_params' => [
                    'email' => 'admin@admin.com',
                    'password' => 'secret'
                ]
            ]
        );

        $response = json_decode($request->getBody()->__toString());

        $this->jwt_token = $response->token;
    }

    public function testGetAll()
    {
        $request = $this->http->get(
            route('api.people', [], false),
            [
                'headers' => [
                    'Authorization' => "Bearer {$this->jwt_token}"
                ]
            ]
        );

        $this->assertEquals(200, $request->getStatusCode());

        $response = json_decode($request->getBody()->__toString());

        $this->assertNotEmpty($response);

        return $response;
    }

    /**
     * @depends testGetAll
     * @param $response
     */
    public function testResultIsValid($response)
    {
        $person = (array) $response[0];

        $this->assertArrayHasKey('id', $person);
        $this->assertArrayHasKey('name', $person);
        $this->assertArrayHasKey('phone', $person);
    }

    public function testGetById()
    {
        $request = $this->http->get(
            route('api.people.show', ['id' => 1], false),
            [
                'headers' => [
                    'Authorization' => "Bearer {$this->jwt_token}"
                ]
            ]
        );

        $this->assertEquals(200, $request->getStatusCode());

        $response = json_decode($request->getBody()->__toString());

        $this->assertNotEmpty($response);
    }

    public function testGetByInvalidId()
    {
        $this->expectException(ClientException::class);

        $request = $this->http->get(
            route('api.people.show', ['id' => 99999], false),
            [
                'headers' => [
                    'Authorization' => "Bearer {$this->jwt_token}"
                ]
            ]
        );

        $this->assertEquals(404, $request->getStatusCode());

        $response = json_decode($request->getBody()->__toString());

        $this->assertEmpty($response);
    }

}
