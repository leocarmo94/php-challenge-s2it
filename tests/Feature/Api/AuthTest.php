<?php

namespace Tests\Feature\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Tests\TestCase;

class AuthTest extends TestCase
{

    /**
     * @var Client
     */
    protected $http;

    protected function setUp()
    {
        parent::setUp();

        $this->http = new Client([
            'base_uri' => env('APP_URL_TESTS')
        ]);
    }

    public function testUnauthorized()
    {
        $this->expectException(ClientException::class);

        $response = $this->http->post(
            route('api.auth', [], false)
        );

        $this->assertEquals(401, $response->getStatusCode());
    }

    /**
     * @return mixed
     */
    public function testAuth()
    {
        $request = $this->http->post(
            route('api.auth', [], false),
            [
                'form_params' => [
                    'email' => 'admin@admin.com',
                    'password' => 'secret'
                ]
            ]
        );

        $this->assertEquals(200, $request->getStatusCode());

        $response = json_decode($request->getBody()->__toString());

        return $response->token;
    }

    /**
     * @depends testAuth
     * @param string $token
     */
    public function testGetAuthUser(string $token)
    {
        $response = $this->http->post(
            route('api.auth.me', [], false),
            [
                'headers' => [
                    'Authorization' => "Bearer $token"
                ]
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * @depends testAuth
     * @param string $token
     */
    public function testTokenRefresh(string $token)
    {
        $response = $this->http->post(
            route('api.auth.token.refresh', [], false),
            [
                'headers' => [
                    'Authorization' => "Bearer $token"
                ]
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetAuthUserUnauthorized()
    {
        $this->expectException(ClientException::class);

        $response = $this->http->post(
            route('api.auth.me', [], false)
        );

        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testGetAuthUserWrongToken()
    {
        $this->expectException(ClientException::class);

        $response = $this->http->post(
            route('api.auth.me', [], false),
            [
                'headers' => [
                    'Authorization' => "Bearer XXX"
                ]
            ]
        );

        $this->assertEquals(401, $response->getStatusCode());
    }

}
