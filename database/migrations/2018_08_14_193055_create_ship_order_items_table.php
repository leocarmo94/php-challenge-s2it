<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ship_order_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ship_order_id')->unsigned();
            $table->foreign('ship_order_id')->references('id')->on('ship_orders')->onDelete('cascade');

            $table->string('title');
            $table->text('note');
            $table->integer('quantity');
            $table->float('price', 8, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ship_order_items');
    }
}
